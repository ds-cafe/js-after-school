/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  main.js is loaded from index.html as an ECMAscript module as follows:
  <script type="module" src="js/main.js"></script>

  This script runs AFTER index.html has completely rendered; this is the
  default behavior that ECMAscript modules are handled by the browser,
  so there is no need to hook into a 'DOMContentLoaded' event

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

/// LIBRARIES /////////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
import * as DIVPRINTER from "./libraries/div-printer";

/// INITIALIZATION ////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// DIVPRINTER.Greeting()
const greetingText = DIVPRINTER.Greeting();

// DIVPRINTER.PrintDiv() adds the passed text to a <div> on the page
DIVPRINTER.PrintDiv(greetingText);
