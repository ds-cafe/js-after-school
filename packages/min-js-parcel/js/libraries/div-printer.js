/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  div-printer.js is an ECMAscript-style module, which you'll have to
  read about in detail elsewhere

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

/// PRIVATE DECLARATIONS //////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const m_name = "Sam Module";

/// PRIVATE HELPERS ///////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/// this function is accessible only from within this module
function m_GetName() {
  return m_name;
}

/// PUBLIC METHODS ////////////////////////////////////////////////////////////
/// These methods will be exported as "named exports"
/// and can be imported using syntax:
/// const { Greeting, PrintDev } = import 'sample-module';
/// They are defined as arrow functions because only arrow functions remember
/// their calling context
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/** Simple function to return a string that is returned from a private method
 */
function Greeting() {
  return `Hello World from ${m_GetName()}`;
}
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/** Utility function to pass string to the 'codeoutput' div
 *  @param {string} str - the string to add to the innerHTML of codeouput
 */
function PrintDiv(str) {
  const div = document.getElementById("codeoutput");
  if (div === undefined) {
    console.log("PrintDiv() could not get #codeoutput element");
    return;
  }
  const strToAdd = `${str}\n`;
  div.innerHTML += strToAdd;
  console.log(`PrintDiv() added ${str} to <div id=codeoutput>`);
}

/// EXPORTS ///////////////////////////////////////////////////////////////////
/// an object of functions and instance properties is exported
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
export { Greeting, PrintDiv };
