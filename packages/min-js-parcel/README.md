[ubuntu10]:https://gitlab.com/ds-cafe/js-after-school/-/wikis/Setup/Installing-NodeJS-for-Ubuntu
[macos]:https://gitlab.com/ds-cafe/js-after-school/-/wikis/Setup/Installing-NodeJS-on-MacOS

# Minimum Modular Javascript Example using Parcel

NOTE: This example requires that you have installed **NodeJS**. If you haven't done that yet, first use [these instructions for Ubuntu, MacOS, and Windows 10][ubuntu10]. You only need to do it once for all example code in `js-after-school` work!

## First Time Initialization

In terminal:
```
cd js-after-school/packages/min-node
nvm use     # looks for a .nvmrc file to set the version of Node
npm ci      # loads precise dependencies specified in package.lock
```
## Running the Program

```
npm start   # runs the 'start' script defined in package.json
```

#### Notable Oddities

- We are still using [normalized css][normalized] as explained in the `min-js` example, except we're now importing it as a module in `main.js`
- We are relying on [Parcel][parceljs] to convert our source code into a "bundled" version that has also be optimized. This is a big topic in itself, but for now you can just look through this example to get an idea of how it's different from the `min-js` example.

[parceljs]: https://parceljs.org
[normalized]: http://nicolasgallagher.com/about-normalize-css#normalize-vs-reset

