/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  This file, library.js, is imported by main.js.

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

/// API ///////////////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/** export a function to collect text for printing to the javascript
 *  console and a div in the document
 */
function printToDiv(str) {
  const div = document.getElementById("codeoutput");
  if (div !== undefined) div.innerHTML += str + "\n";
  else console.log("failed to print:", "'" + str + "'");
}
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
function helloWorld() {
  printToDiv('Hello, World!');
}

/// EXPORTS ///////////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/// named exports are imported as `import { printToDev
export {
  printToDiv,
  helloWorld
}
