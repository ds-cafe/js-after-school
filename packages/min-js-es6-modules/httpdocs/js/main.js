/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  main.js is loaded from index.html as type="module" as in:
  <script src="js/main.js" type="module"></script>

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

import { printToDiv, helloWorld } from  './library.js';

/// INITIALIZATION ////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
console.log('initialized main.js');

/// RUNTIME CODE //////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/** Unlike the example in 'packages/min-js', we can be assured that the
 *  imports have been resolved before we invoke them, so we don't need to
 *  listen for DOMContentLoaded. Also, since this file is loaded as type
 *  "module", it is only invoked after the page is loaded.
 *  See flaviocopes.com/javascript-async-defer/#the-position-matters
 */
helloWorld();
printToDiv("script in main called imported printToDiv()")
