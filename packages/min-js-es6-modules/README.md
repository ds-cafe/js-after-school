# Minimum Javascript Example using ES Modules

This demonstrates the use of [ES Modules][modules] without using a bundler like Parcel or Webpack. ES Modules were introduced in EcmaScript 2016 (aka [ES6](https://en.wikipedia.org/wiki/ECMAScript#6th_Edition_%E2%80%93_ECMAScript_2015))

## Running the Demo

When running for the first time, cd `packages/min-js-es6-modules` and then `npm ci` (assumes you have set up your environment as described in [Setup](https://gitlab.com/ds-cafe/js-after-school/-/wikis/Setup/Installing-NodeJS-for-Ubuntu)).

There are two webservers you can use:
```
npm run server     # uses 'serve' github.com/vercel/serve
npm run alt-server # uses the provided `server-express.js`
```
Then point your browser to the URL shown in the terminal.

#### Notable Oddities

- CSS values are ["normalized"][normalized].
- For features like importing ES modules, it doesn't make sense to support Internet Explorer. This is not a 'shimable' solution, so we do NOT use [HTML5 Shiv][shiv]. You must use a [supported browser](https://caniuse.com/es6-module).
- If you do need to support pre-ES6 browsers, you should use a [module bundler](https://stackoverflow.com/questions/38864933) which will 'transpile' your fancy modern code into compatible code.

[normalized]: http://nicolasgallagher.com/about-normalize-css#normalize-vs-reset
[modules]:https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules
[shiv]:https://www.paulirish.com/2011/the-history-of-the-html5-shiv/
