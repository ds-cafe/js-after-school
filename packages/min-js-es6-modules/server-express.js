/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  Example of simple Node application that implements a web server using
  Express.

  Helpful Concepts

  * A basic primer to HTTP Transactions in Node:
    nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/

  * Express 'app' object documentation:
    expressjs.com/en/4x/api.html#app
    see the get and post methods

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

/// LIBRARY IMPORTS ///////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const Path = require("path"); // built-in Node path utility
const Express = require("express"); // external web server package
const IP = require("ip"); // external ip address utility package

/// DECLARATIONS //////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const SRV_IP = IP.address();
const SRV_PORT = 3000; // port to listen for http traffic
const SRV_FILES = Path.resolve(__dirname, "httpdocs");

/// SUPER SIMPLE WEBSERVER ////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/// create Express app
const app = Express();

/// Add 'Express.static' middleware to serve static files for a simple plain
/// file webserver. Put your HTML files in directory specified by SRV_FILES
app.use("/", Express.static(SRV_FILES));

/// start web server
app.listen(SRV_PORT, () => {
  console.log(`ex-express webserver listening at ${SRV_IP}:${SRV_PORT}`);
  console.log("control-c to stop server");
});
