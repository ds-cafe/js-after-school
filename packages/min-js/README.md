# Minimum Javascript Example with Direct Authoring

This is a set of source files that load Javascript. You can drag `index.html` into a web browser to see it work, or just upload the contents to a web server and it will work. The only tools you need are a text editor to edit the various
source files.

#### Notable Oddities

- For consistency across browsers, the default CSS values are ["normalized"][normalized] by importing `normalize.css` into the main stylesheet `style.css`. This practice started around 2012 to eliminate the small rendering differences between different browsers. It is also known as doing a "CSS Reset"
- An [HTML5 Shiv][shiv] is used to hack around problems that old versions of Internet Explorer had with HTML5 back in 2008. While it's arguable that this is not necessary in 2021, it's often still included in HTML5 boilerplate templates.
- On a related note, `library.js` uses an archaic string concatenation technique instead of using ["template literals"](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals). However, in a vanilla Javascript you don't know if the browser you're using supports it, as it was introduced with ES6 aka ECMAScript2015, so you're forced to use the lowest common denominator version you expect to have. This sucks, so build tools like Babel can "transpile" source using the latest ECMAScript goodies into earlier versions supported by all browser. But this example is vanilla, so you don't get that.

[normalized]: http://nicolasgallagher.com/about-normalize-css#normalize-vs-reset
[shiv]:https://www.paulirish.com/2011/the-history-of-the-html5-shiv/
