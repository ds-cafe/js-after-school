/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  Example of simple Node application that implements a web server using
  Express's router helper. This allows you to create route handlers that
  use different middleware depending on the url!

  Helpful Concepts

  * A basic primer to HTTP Transactions in Node:
    nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/

  * Express 'app' object documentation:
    expressjs.com/en/4x/api.html#app
    see the get and post methods

  * Express 'router' object documentation:
    expressjs.com/en/4x/api.html#router

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

/// LIBRARY IMPORTS ///////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const Path = require("path"); // built-in Node path utility
const Express = require("express"); // external web server package
const IP = require("ip"); // external ip address utility package

/// DECLARATIONS //////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const SRV_IP = IP.address();
const SRV_PORT = 3000; // port to listen for http traffic
const SRV_FILES = Path.resolve(__dirname, "static");

/// SUPER SIMPLE WEBSERVER ////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/// create Express app
const app = Express(); // this also the default router

/// create two routers and define their routes
const userRouter = Express.Router();
const adminRouter = Express.Router();

userRouter.get('/user', (req,res,next)=> {
  console.log('user router is working'); res.end();
});
adminRouter.get('/admin', (req,res,next)=> {
  console.log('admin router is working'); res.end();
});

/// add routers as middleware to main app
app.use(userRouter);
app.use(adminRouter)

/// Add 'Express.static' middleware to serve static files for a simple plain
/// file webserver. Put your HTML files in directory specified by SRV_FILES
app.use("/", Express.static(SRV_FILES));

/// start web server
app.listen(SRV_PORT, () => {
  console.log(`ex-express-router server listening at ${SRV_IP}:${SRV_PORT}`);
  console.log("control-c to stop server");
});
