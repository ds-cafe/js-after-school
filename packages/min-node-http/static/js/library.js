/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  This file, library.js, is loaded by index.html before main.js. The load
  is guaranteed to be synchronous, each file completely loading before
  the next one in the order they appear.

  In the vanilla browser javascript environment, you may be surprised to
  learn tht all script runs in a global context, so the function `printToDiv`
  is accessible from main.js despite being defined in a separate file.

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

/// DECLARATION ///////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/** define a function to collect text for printing to the javascript
 *  console and a div in the document
 */
function printToDiv(str) {
  const div = document.getElementById("codeoutput");
  if (div !== undefined) div.innerHTML += str + "\n";
  else console.log("failed to print:", "'" + str + "'");
}
