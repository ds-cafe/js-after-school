[ubuntu10]:https://gitlab.com/ds-cafe/js-after-school/-/wikis/Setup/Installing-NodeJS-for-Ubuntu
[macos]:https://gitlab.com/ds-cafe/js-after-school/-/wikis/Setup/Installing-NodeJS-on-MacOS

# Minimum NodeJS HTTP Examples

NOTE: This example requires that you have installed **NodeJS**. If you haven't done that yet, first use [these instructions for Ubuntu, MacOS, and Windows 10][ubuntu10]. You only need to do it once for all example code in `js-after-school` work!

## First Time Initialization

In terminal:
```
cd js-after-school/packages/min-node-http
nvm use        # looks for a .nvmrc file to set the version of Node
npm ci         # installs dependencies described in package-lock.json
```

## Running the Program
```
npm run ex-http             # runs a web server at port 3000, using the built-in Node http module
npm run ex-express          # runs web server at 3000, using ExpressJS to serve static files 
npm run ex-express-router   # runs web server at 3000, using ExpressJS to serve static files 
```
Now open a web browser and go to the address listed in the terminal output. It will look something like `192.168.1.184:3000`, so I would browse to `htttp://192.168.1.184:3000`; your address will probably be different.

You should see an index page appear!

## Notable Files and Folders

This is a more complicated Node project that are dependent on several external modules. These "dependencies", along with other information, is stored in the `package.json` file. The `package-lock.json` is created when first using npm to install packages, and `node_modules` contains the external modules that you have installed.

Here's what in e

### • `package.json`

This is the key configuration file for any NodeJS project. This file contains information for various package-related purposes. This project uses a very minimal one:
```
{
  "name": "package-example",
  "version": "0.0.1",
  "description": "minimum nodejs project using package manager",
  "main": "index.js",
  "scripts": {
    "app": "node app.js"
  },
  "keywords": [],
  "author": "DSri Seah",
  "license": "MIT",
  "dependencies": {
    "express": "^4.17.1",
    "ip": "^1.1.5"
  }  
}
```
#### key settings in `package.json`
There are dozens of potential settings you might find in `package.json`, but here are some basic ones:

* **`scripts`** are named snippets of text that is passed to the command line. In this example, `npm run app` executes `node app.js`, which starts the program. 
* **`dependencies`** lists the external packages that this project requires to run. They are added with the `npm install --save` command and include the version of the packages that were installed by it. `npm` also installs the dependencies of your packages.
* **`main`** describes the entry point if your project is published as a package on the npm registry. It is superfluous here since this is not a published project. However, it's important to note that this has nothing to do with what happens when you run your project on the command line; that's what our "example" script is for.

### • `node_modules/`

This directory contains the packages you added with the `npm install` command. You will find the `express` package in `node_modules/express` and `ip` at `node_modules/ip`. You'll also find a whole lot of other packages; these are the packages that `express` and `ip` have as direct dependencies. If you look inside `node_modules/express`, you'll see that it has its own `package.json` just like this project.

### • `package-lock.json`

The dependencies between packages can potentially have different versions of the same package, so a "dependency tree" is generated and stored in `package-lock.json`. This allows the npm to recreate the _exact_ same tree when reinstalling, which keep the relationships between packages the same as well. 

### • `static/`

This folder contains files that are served by the ExpressJS webserver created in `app.js`. These are the files you know and love from hosting websites on other servers. This is a project-specific folder related to your code, not npm.

