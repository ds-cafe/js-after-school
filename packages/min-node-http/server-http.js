/*///////////////////////////////// ABOUT \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*\

  Example of simple Node application that implements a web server using
  Node's built in http library

  Helpful Concepts
  A basic primer to HTTP Transactions in Node:
  nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/

\*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ * /////////////////////////////////////*/

/// LIBRARY IMPORTS ///////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const http = require('http'); // built-in http handling
const fs = require('fs'); // built-in file handling
const path = require('path'); // built-in filename and directory utilities

/// DECLARATIONS //////////////////////////////////////////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const SRV_PORT = 3000; // port to listen for http traffic

/// BASIC WEBSERVER WHERE YOU HAVE TO DO ALL THE WORK /////////////////////////
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/// create http server event emitter
const server = http.createServer();

/// subscribe an event handler to 'request' event
/// this will serve files as-is from the static/ directory
server.on('request', (req, res) => {
  // for a list of http methods for request/response, see
  // developer.mozilla.org/en-US/docs/Web/HTTP/Methods
  let { method, url } = req;
  if (url==='/') url='/index.html';
  // handle http GET
  if (method==='GET') {
    const file = __dirname + '/static' + url;
    console.log('GET', url);
    fs.readFile(file, function callback(err,data) {
      if (err) {
        res.writeHead(404);
        res.end(JSON.stringify(err));
        return;
      }
      res.writeHead(200);
      res.end(data);
    });
  }
});

/// start the server
server.listen(SRV_PORT,err=>{
  if (err) throw Error(err);
  console.log(`ex-http server listening on port ${SRV_PORT}`);
});
