[ubuntu10]:https://gitlab.com/ds-cafe/js-after-school/-/wikis/Setup/Installing-NodeJS-for-Ubuntu
[macos]:https://gitlab.com/ds-cafe/js-after-school/-/wikis/Setup/Installing-NodeJS-on-MacOS

# Minimum NodeJS Example 

NOTE: This example requires that you have installed **NodeJS**. If you haven't done that yet, first use [these instructions for Ubuntu, MacOS, and Windows 10][ubuntu10]. You only need to do it once for all example code in `js-after-school` work!

## Running the Programs

All the simple examples in `min-node` can be run using the node interpreter.

Try each line by itself in the terminal.
```
node 01_hello      # prints "Hello World" to the console
node 02_script     # prints current git branch
./02_script        # use #!/usr/bin/env node to executable script
```

#### Notables

These simple examples use only built-in [Node API](https://nodejs.org/api) objects that are part of the runtime environment. To use external modules from the main [npm registry](https://npmjs.com), see the examples in `min-node-package`.

