## About Javascript After School

`js-after-school` (JSAS) is a room in the [DS|CAFE Virtual Coworking Discord](https://davidseah.com/virtual-coworking). The room is inspired by the feeling of learning how to do things in the computer lab after school, sharing tips in a friendly environment where generosity with our knowledge and enthusiasm is the key cultural value!

The idea for this repo is to make a collection of material that helps curious programmers do things in Javascript as quickly as possible. It's presumed that you already know how to create moderately-complex programs in another language environment, but are ignorant of what conventions and practices carry over and what is uniquely weird.

## What's New

Right now you can:

* Browse the reference material being compiled in the [JSAS Wiki](https://gitlab.com/ds-cafe/js-after-school/-/wikis/home).
* Help guide the scope and mission of JSAS in [this issue](#1).

## Repo Organization

The `packages` directory contains stand-alone "ready to run" examples. So far we have:

* `min-js`              - ES5 Javascript in a browser
* `min-js-es6-modules`  - Use ES6 Modules in browser w/ server
* `min-js-parcel`       - using a "module bundler" to combine and build files
* `min-node`            - Javascript in NodeJS, only internal modules
* `min-node-http`       - Simple web servers with NodeJS
* `express`             - ExpressJS projects
